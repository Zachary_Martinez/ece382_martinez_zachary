#include <msp430.h>

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;
    volatile unsigned char val1 = 0x40;
	volatile unsigned char val2 = 0x35;
	volatile unsigned char val3 = 0x42;
	
	volatile unsigned char result1 = 0;
	volatile unsigned char result2 = 0;
	volatile unsigned char result3 = 0;

	volatile const int Threshold = 0x38;

	if(val1 > Threshold){
		int temp, count;
		int fib2 = 1;
		int fib1 = 0;
		for(count = 2; count<10; count++) {
			temp = fib2;
			fib2 = fib1 + fib2;
			fib1 = temp;
		}
		result1 = fib2;
	}

	if(val2 > Threshold){
		result2 = 0xAF;
	}

	if(val3 > Threshold){
		result3 = val2 - 0x10;
	}

    while(1){}
}
