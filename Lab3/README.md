# Lab 3 - SPI - "I/O"

#

## By C2C Zach Martinez
 
### Objectives
The goal of this lab is to learn input and output processes using the msp430 and the LCD screen.

### Preliminary design

##  Mega Prelab
A hard copy of this Mega Prelab is required to be turned in as well as pushed to Bitbucket.  Answers should not be handwritten.  The timing diagram may be NEATLY drawn by hand with the assistance of a straightedge on engineering paper.

### Delay Subroutine
In lab3_given.asm, you have the header for a subroutine (line 579), but there is no code.  Write a subroutine that will create a 160ms delay.  Show your analysis that proves the delay is indeed very close to 160 ms.  Note: the clock rate is set to 8 MHz (see the first two lines of initMSP).

	the code that I used for the delay function is as follows.
				call #Delay160ms	;5 cycles
		Delay160ms:
	
				push r5				;3 cycles
				push r6				;3 cycles
				mov.w #0x10, r6		;2 cycles
	
		bigdelay:
				mov.w #0xad00, r5	;2 cycles
			
		delay:	
				dec r5				;1 cycle
				jnz delay			;2 cycles
			
				dec r6				;1 cycle
				jnz bigdelay		;2 cycles
			
				pop r6				;2 cycles
				pop r5				;2 cycles
				ret					;3 cycles
	the total number of clock cycles for this function is:
		5 + 3 + 3 + 2 + 2 + [[(0xad00 * (1 + 2)) + 1 + 2] * 10] + 2 + 2 + 3 =  1328692 cycles

	With an 8 MHz signal, the delay comes out to be 166 ms. After testing this routine in the logic analyzer, i found the delay to be 177 ms. To fix this, I reduced the outer loop from 10 to 6, which gave me a value of 159.75 ms. This discrepenecy and my actual delay is due to the MSP 430 not having an actual clock speed on 8 MHz but 8.317 MHZ.


![Lab3Delay](Images/Lab3Delay.png)


### ILI9341 LCD BoosterPack 

Look at the schematic for the LCD BoosterPack. Complete the following table.  The pin number (1 - 20) should be the pin number that signal connects to on the MSP 430, and the PX.X should be the pin and port it connects to (e.g. P1.0). <br>

	| Name 	| Pin # | PX.X	|
	|	:-: | :-: 	|:-: 	|
	| S1 	|   5	| P1.3 	|
	| S2 	|   8	| P2.0	|
	| S3 	|   9	| P2.1	|
	| S4 	|	10	| P2.2	|
	| S5	|	11	| P2.3 	|
	| MOSI	| 	15	| P1.7	| 
	| CS 	| 	2	| P1.0	|
	| DC 	| 	6	| P1.4	|
	| MISO	| 	14	| P1.6	|

What are the hex values that need to be combined with the below registers for these signals to be properly configured?  State whether that hex value needs to be used with the bis or bic instruction with each register to achieve these ends.  If the register is not affected for that signal, simply say N/A. 

	|Signal	|PxDIR	|PxREN	|PxOUT	|PxSEL	|PxSEL2 |
	|:-:	|:-:	|:-:	|:-:	|:-:	|:-:	|
	|S1		|bic 08	| bis 08| bis 08| n/a   | n/a   |
	|MOSI	| n/a	| n/a   | n/a   | bis 80| bis 04|
	|CS		|bis 01	| n/a   | bis 01| n/a   | n/a	|



### Configure the MSP430

Look at the initMSP subroutine in the lab3_given.asm file.  There are four pins being intialized on port 1: SCLK, CS, MOSI, and DC.  What is the pin number (1-20) associated with each of these signals?  What function does each signal serve?  For example, SCLK is the serial clock. 

	| Name 	| Pin # | Function  			|
	|:-:	|:-:	|:-:					|
	| SCLK 	|   7	| serial clock  		|
	| CS 	|   2	| Chip select			|
	| MOSI 	|   15	| Master out, Slave in 	|
	| DC 	|	6	| Data or command		|

Below the pin configuration code are some lines of code from the lab3_given.asm file (lines 136 - 143) to properly configure the SPI subsystem.  Use this code to answer the next two questions.

	1:		bis.b	#UCSWRST, &UCB0CTL1
	2:		mov 	#UCCKPH|UCMSB|UCMST|UCSYNC, &UCB0CTL0
	3:		bis 	#UCSSEL_2, &UCB0CTL1
	4:		bis 	#BIT0, &UCB0BR0
	5:		clr	&UCB0BR1
	6:		bis	#LCD_SCLK_PIN|LCD_MOSI_PIN|LCD_MISO_PIN, &P1SEL
	7:		bis	#LCD_SCLK_PIN|LCD_MOSI_PIN|LCD_MISO_PIN, &P1SEL2
	8:		bic	#UCSWRST, &UCB0CTL1

Fill in the chart below with the function that is enabled by the lines 6&7 of the above code.  Your device-specific datasheet can help.

	| Pin name 		|		 Function 	| 
	|	:-:			|		:-:			|
	| P1.5			| Set as the clock	|
	| P1.7			|Slave in Master out|
	| P1.6	 		|Master in slave out|

Next, describe specifically what happens in each of the eight lines of code above.  Line 1 and 3 have been done for you as an example. <br>

	Line 1: Setting the UCSWRST bit in the CTL1 register resets the subsystem into a known state until it is cleared.
	Line 2: the control register is set to have clock phase, most significant bit first, master, and sync all enabled.
	Line 3: The UCSSEL_2 setting for the UCB0CTL1 register has been chosen, selecting the SMCLK (sub-main clock) as the bit rate source clock for when the MSP 430 is in master mode. <br>
	Line 4: The bit rate is 1 so no clock scaling occurs
	Line 5: bit rate out is still 1
	Line 6: pins 1.5/6/7 will have the first bit selcted for use as identified above. this value will be used in a multiplexor
	Line 7: pins 1.5/6/7 will have the second bit selcted for use as identified above. this value will be used in a multiplexor 
	Line 8: the ctl1 register will no longer be reset.

### Communicate with the LCD
The following code (lines 297 - 338) sends one byte (either data or command) to the TM022HDH26 display using its 8-bit protocol.  

	;-------------------------------------------------------------------------------
	;	Name: writeCommand
	;	Inputs: command in r12
	;	Outputs: none
	;	Purpose: send a command to the LCD
	;	Registers: r12 preserved
	;-------------------------------------------------------------------------------
	writeCommand:
		push	r12
		bic 	#LCD_CS_PIN, &P1OUT
		bic		#LCD_DC_PIN, &P1OUT
		mov.b	 r12, &UCB0TXBUF
	
	pollC:
		bit	#UCBUSY, &UCB0STAT	;while UCBxSTAT & UCBUSY
		jnz		pollC
	
		bis		#LCD_CS_PIN, &P1OUT
		pop		r12
		ret
	
	;-------------------------------------------------------------------------------
	;	Name: writeData
	;	Inputs: data to be written in r12
	;	Outputs: none
	;	Purpose: send data to the LCD
	;	Registers: r12 preserved
	;-------------------------------------------------------------------------------
	writeData:
		push	r12
		bic 	#LCD_CS_PIN, &P1OUT
		bis	#LCD_DC_PIN, &P1OUT
		mov.b 	r12, &UCBxTXBUF
	
	pollD:
		bit	#UCBUSY, &UCBxSTAT	;while UCBxSTAT & UCBUSY
		jnz	pollD
	
		bis	#LCD_CS_PIN, &P1OUT
		pop	r12
		ret


Use this code to draw two timing diagrams (one for each subroutine) of the expected behavior of LCD_CS_PIN, LCD_DC_PIN, LCD_SCLK_PIN, and UCBxTXBUF from the begining of these subroutines to the end.  Make sure that you clearly show the relationship of the edges in the clk and data waveforms. 

![writecom](Images/writecom.PNG)

![writedata](Images/writedata.PNG)


### Draw a pixel
The following code (lines 552 - 576) draws a pixel of a predetermined color at the coordinate (R12, R13).  However, four subroutines are called to execute this seemingly simple task.  Explain the purpose of each of the four subroutine calls:

	|Subroutine	| Purpose	|
	|:-:		|:-:		|
	|setArea	|Defines area where image will exist|
	|splitColor	|stores 1 color value in 2 registers|
	|writeData	| sends 1st color data to the slave	|
	|writeData	| sends 2nd color data to the slave	|


	;-------------------------------------------------------------------------------
	;	Name: drawPixel
	;	Inputs: x in r12, y in r13, where (x, y) is the pixel coordinate
	;	Outputs: none
	;	Purpose: draws a pixel in a particular spot
	;	Registers: r12, 13, 14, 15 preserved
	;-------------------------------------------------------------------------------
	drawPixel:
		push	r12
		push	r13
		push	r14
		push	r15
		mov.b	r12, r14
		mov.b	r13, r15
		call	#setArea
		mov		#COLOR1, r12
		call	#splitColor
		call	#writeData
		mov		r13, r12
		call	#writeData
		pop		r15
		pop		r14
		pop		r13
		pop		r12
	ret

(This marks the end of the Mega Prelab.)


### Logic Analyzer Section

#### Overview
	The set area function that I am analyzing takes values that are stored in registers before hand and uses those to tell the LCD what the boundaries of the display will be. 
	A command is sent out to the LCD to prepare the LCD for the type of information that will be recieved, and then 4 data packets are sent to the LCD. 
	Each data packet contains 1 byte which holds one half of the x value or y value for each corner. 
	A final command is sent out to tell the LCD that all the information is recieved.  

![Lab3Overview](Images/Lab3Overview.png)

###Table

	|Packet	|Line	|Command/Data	|8-bit packet	|			Meaning of packet							|
	|1		|	439	|	Command		|	00101010	|	Tells LCD Column information is coming				|
	|2		|	442	|	Data		|	11111111	|	X Start MSB = 0x0									|
	|3		|	445	|	Data		|	01010110	|	X Start LSB = 0xA6									|
	|4		|	449	|	Data		|	11111111	|	X End MSB = 0x0										|
	|5		|	452	|	Data		|	01010110	|	X End MSB = 0xA6									|
	|6		|	455	|	Command		|	00101011	|	Tells LCD Row information is coming					|
	|7		|	459	|	Data		|	00000000	|	Y Start MSB = 0x0									|
	|8		|	462	|	Data		|	00010100	|	Y Start LSB = 0xDE									|
	|9		|	467	|	Data		|	00000000	|	Y End MSB = 0x0										|
	|10		|	470	|	Data		|	00010100	|	Y End LSB = 0xDE									|
	|11		|	473	|	Command		|	00101100	|	Tells LCD that bits are done transfering for now	|

![Lab3DataCom](Images/Lab3DataCom.png)

###Analysis

Looking back to the overview waveform, The signals that are given do make sense for the assembly code that is written. The command/Data line switches to the appropriate value in the order that the program outlines (1 command call, 4 data calls, 1 command call, 4 data calls, 1 command call) The serial clock shows increments only when data is being transferred, which is shown in the MOSI waveform.


### Documentation
None outside given resources.