## Prelab (25 pts)

C2C Zach Martinez

You need to provide the following:

1. Answers to all **bulleted** questions below
2. Completed ![Lab6_schematic](Lab6_schematic.png)
3. Software flowchart and/or pseudo-code


Consider your hardware (timer subsystems, chip pinout, etc.) and how you will use it to achieve robot control.

- Which pins will output which signals you need?
	- Pin 2.0 will enable both motors, Pin 2.1 will determine  the high or low state of the left motor for forward motion, Pin 2.2 will determine the high or low state of the right motor for forward motion. Pin 2.3 will determine  the high or low state of the left motor for backwards motion, Pin 2.4 will determine the high or low state of the right motor for backwards motion.
- Which side of the motor will you attach these signals to?
	- Pin 2.1 will go through the amplifier through to the positive side of the left motor. Pin 2.2 will go through the amplifier through to the positive side of the right motor. Pin 2.3 will go through the amplifier through to the negative side of the left motor. Pin 2.4 will go through the amplifier through to the negative side of the left motor.

- How will you use these signals to achieve forward / back / left / right movement?
	- The magnitude of the signal will be determined by the duty cycle of the PM signal. To accomplish a forward movement, pins 2.1 and 2.2 will output the same signal with the same duty cycle. To accomplish a backwards movement, pins 2.3 and 2.4 will output the same signal with the same duty cycle. To accomplish a left turn, pins 2.3 and 2.2 will output the same signal with the same duty cycle.  To accomplish a right turn, pins 2.1 and 2.4 will output the same signal with the same duty cycle

**Spend some time here, as these decisions will dictate much of how difficult this lab is for you.**


Consider how you will setup the PWM subsytem to achieve this control.

- What are the registers you'll need to use?
	- ~
- Which bits in those registers are important?
	- ~
- What's the initialization sequence you'll need?
	- Pins 2.0 through 2.4 will need to be enabled as output pins. The timer a interrupt sequence will also be used in order to create a PWM signal.



Consider what additional hardware you'll need (regulator, motor driver chip, decoupling capacitor, etc.) and how you'll configure / connect it.

Complete the [Lab 6 schematic](Lab6_schematic.pdf).  Include a neat picture in your README.

Consider the software interface you'll want to create to your motors.

- Do you want to move each motor invidiually (`moveLeftMotorForward()`)?  Or do you want to move them together (`moveRobotForward()`)?
	- I will create helper functions that move each motor in a specific direction and then have higher functions that utilize the helper functions to carry out a specific movement. This will make it easier to debug and check that my robot and code are working properly.


Include whatever other information from this lab you think will be useful in creating your program.


####PsuedoCode 

	• Include "Lab6Header.H"
	• Main
		○ InitializeMSP430
		○ runMoveDemo
	• InitializeMSP430
		○ enablePinsOutputs
		○ setupInterruptRegisters
		○ PWM = createPWMsignal
	• runMoveDemo
		○ moveForward
		○ turnLeft
		○ moveBack
		○ turnRight
	• moveForward
		○ moveLeftMotorForward
		○ moveRightMotorForward
	• moveBack
		○ moveLeftMotorBack
		○ moveRightMotorBack
	• turnLeft
		○ moveLeftMotorBack
		○ moveRightMotorForward
	• turnRight
		○ moveLeftMotorForward
		○ moveRightMotorBack
	• moveLeftMotorForward
		○ Apply PWM to PIN 2.1
	• moveRightMotorForward
		○ Apply PWM to PIN 2.2
	• moveLeftMotorBack
		○ Apply PWM to PIN 2.3
	• moveLeftMotorBack
		○ Apply PWM to PIN 2.4
