# Assignment 2 - Addressing Modes, Hand-assembly

## Addressing Modes

**Name: C2C Zach Martinez**


**Section: M1A**


**Documentation: None**


**All answers should be in little-endian, hex format.**

1. (10pts each) For each of the following instructions, identify the addressing modes being used.  For the indicated instructions, write the hand-assembled machine code that corresponds to that instruction.
	
```
swpb    r7
```

	Addressing mode used for source: N/A

	Addressing mode used for destination: Register Mode

	hand-assembled machine code (**in little-endian, hex format**): 0001 0000 1100 0111 = 0x10C7
```
xor     @r12+, 0(r6)
```

	Addressing modes used for Source: Indirect autoincrement

	Addressing mode used for destination: Index

	hand-assembled machine code (**in little-endian, hex format**): 1110 1100 1011 0110 0000 = 0x ECB60
```
nop
```
	
	Addressing mode used for source: Register

	Addressing mode used for destination: Register

	hand-assembled machine code (**in little-endian, hex format**): 0100 0011 0000 0011 = 0x4303
```
mov.w   @r12, &0x0200
```

	Addressing mode used for source: Indirect Register

	Addressing mode used for destination: Absolute

	hand-assembly of `mov @r12, r6`: 0100 1100 1110 0000 0000 0010 0000 0000 = 0x4CE0 0200 

</ol>
<ol start="2">
<li> (5pts) Consider the following code snippet:
</ol>

	jmp			TARGET	; address is 0xc000
	...
	TARGET:     nop		; address is 0xc024

What addressing mode do relative jumps use (if any)?
	
	Jumps use immediate addressing

</ol><ol start="3">
<li> (20pts) Consider the following code snippet:

	mov.w   #0xFF, P1OUT    ;P1OUT is 0x0021

a) (5pts) What addressing modes are being used here?

	Addressing mode used for source: Immediate

	Addressing mode used for destination: Absolute


b) (10pts) The programmer wants this code to move 0xFF into P1OUT, but the code isn't working.  Why not?  
*Hint: talk about the assembly / linking process.*

	When the code is assembled into object code the address of all the commands are made with the first line at 0x0000. when they files are linked, the starting location is rearranged so all the code can be ran in the proper order. using absolute addressing modes causes the program to point to a specific location in memory that does not change when linked, meaning the command is pointing to something different than intended.

c) (5pts) How would you change the code snippet to fix the problem?
	
	I would use indexed mode and use the command 15(R6) to point to 0x0021

</ol> <ol start="4"> <li> (10pts) Consider the following code snippet:

	mov.w   r10, @r9

a) (5pts) This doesn't assemble.  Why?

	Indirect register mode cannot be used to point to a destination. 

b) (5pts) What's an equivalent replacement instruction?
	
	mov.w	r10 0(r9)

</ol> <ol start="5"> <li> (5pts)What is the purpose of emulated instructions?

	to provide a level of abstraction for the programmer. this means less hardware is required to perform the same amount of commands.

</ol> <ol start="6"> <li> (20pts)Use the MSP430x2xx Family User's Guide to answer the following questions:

a) (5pts) What status bits does the TST instruction manipulate?

	All of them, V N Z C

b) (10pts) In the example code for the CMP instruction (page 77) what role do the R6 and R7 registers play?

	 R7 is subtracted from R6. if the difference is 0, then they equal and the PC advances to the EQUAL label.

c) (5pts) In the Digital I/O section, how is it recommended that you should configure unused pins?  In your own words, explain why is this course of action recommended.

	They should be left as I/O pins in the output setting but left unconnected. This prevents stray inputs from entering the system and keeps the power draw of the system low since the system isn't constantly reading for inputs. c