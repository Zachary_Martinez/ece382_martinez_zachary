# ECE382 Skills Review

**Name: C2C Zach Martinez**
<br>
<br>
**Section: M1A**
<br>
<br>
**Documentation: None**
<br>
<br>
Due Date: **Beginning of Class (BOC), Lesson 3**

**Authorized Resources**

This review is **individual effort** and will be counted as a quiz grade.  You may seek help from any instructor and reference any publication in its completion.  You may **not** reference ECE382 skills reviews from previous years.  Normal documentation is required.

## Part 1: Software

Install the Code Composer Studio (CCS) Integrated Development Environment (IDE) using the instructions from [Computer Exercise 1](/382/labs/compex1/index.html) - the "Installing Code Composer Studio (CCS)" block .  Do not do anything else from the computer exercise.

This will not be checked, but **it must be accomplished by BOC on Lesson 5**.  Failure to accomplish this will result in a **50% penalty** on this assignment.

## Part 2: Introduction to Numbering Systems

Objectives:


- Explain, by means of an example, the idea of positional weighting in numbering systems.
- Convert an arbitrary base Q number to a decimal number.
- Convert a decimal number to a number with base Q.


1. (5pts each) Convert the following numbers to decimal numbers.  **You must show your work for credit**.

    a. D209 (base 16)
<br>
<br> 	D209 = 
<br>	= (D * 16^3) + (2 * 16^2) + (0 * 16^1) + (9 * 16^0) 
<br>	= (13 * 4096) + (2 * 256) + 0 + 9
<br>	= 53769
<br>

    b. 40A7 (base 11)
<br>
<br> 	40A7 = 
<br>	= (4 * 11^3) + (0 * 11^2) + (A * 11^1) + (7 * 11^0) 
<br>	= (4 * 1331) + 0 + (10 * 11) + 7
<br>	= 5441
<br>

    c. 672 (base 8)
<br>
<br> 	672 = 
<br>	= (6 * 8^2) + (7 * 8^1) + (2 * 8^0) 
<br>	= (6 * 64) + (7 * 8) + 2
<br>	= 442
<br>

    d. 10101110 (base 2)
<br>
<br>	1010 = A    1110 = E
<br>	A * 16 + E = 10101110
<br>	= 10 * 16 + 14
<br>	= 174
<br>

2. (5pts each) Convert the following numbers to the base(s) indicated.

    a. 547 (base 10) to base 2 and base 16
<br>
<br>	547 - (512) = 35 ; 35 - (32) = 3 ; 3 - (2) = (1)
<br>	547 = 001000100011
<br>	0010 0010 0011 = 223x16
<br>

    b. B36D (base 16) to base 2 and base 8
<br>
<br>	B = 1011 ; 3 = 0011 ; 6 = 0110 ; D = 1101
<br>	B36D = 001011001101101101
<br>	= 001 011 001 101 101 101
<br>	=  1   3   1   5   5   5
<br>	= 131555x8
<br>
## Part 3: Representation of Negative Numbers

Objectives:


- Understand the concept of a sign bit.
- Understand the concept and uses of two's complement numbers.


<ol start="3">
<li> (5pts each) Find the 8-bit two's complement representation of the following numbers.
<br>
    a. 116 (base 10)
<br>
<br>	116 - (64) = 52 - (32) = 20 - (16) = (4)
<br>	116 = 01110100
<br>
<br>
<br>
    b. -37 (base 10)
<br>
<br>	37 - (32) = 5 - (4) = (1)
<br>	37 = 00100101
<br>	-37= 11011011
<br>
<br>
    c. -2 (base 10)
<br>
<br>	2 = 00000010
<br>	-2= 11111110
<br>
<br>
<br>
<li> (5pts) What are the largest (most positive) and the smallest (most negative) 8-bit two's complement numbers in hexadecimal **and** decimal formats?
<br>
<br> Largest is 127 (7F), Smallest is -127 (80)
<br>							 
<br>
</ol>
## Part 4: Binary Addition and Subtraction, Concept of Overflow

Objectives:


- Explain the concept of overflow and when it occurs.
- Know how to add and subtract two binary numbers and note whether an overflow occurs.

<ol start="5">
<li> (5 pts each) All of the numbers below are represented using 8-bit, two's complement notation.  Perform the specified operations and specify whether or not an overflow occurs.

<br>
a.  
&nbsp; &nbsp; 1110 1101  
+ 1010 1100  
<br>   	11101101 + 10101100 = (1) 00011001
<br>	An overflow has occured
<br>
<br>
<br>
b.  
&nbsp; 1001 1000  
- 1110 0111  
<br>
<br>  10011000 - 11100111 = 10110001
<br>  10011000 = - 01101000 = -104
<br>  11100111 = - 00011001 = -25
<br> - 104 - (-25) = - 79 = - 01001111 = 10110001
<br> No Overflow occured
<br>
<br>
<li> (5pts) When an overflow occurs, a common solution is to expand the number of bits used in the calculation (known as sign extension).  How would  you extend the negative 8-bit two's complement number with a hexadecimal value of BC to 16 bits?
<br>
<br>	BC = 1011 1100 = - 01000100 = - 68
<br>    68 = 0000 0000 0100 0100 
<br>	-68 = 1111 1111 1011 1100
<br>
<br>
<li> (10pts each) Perform the specified operations and specify whether or not an overflow occurs.  Give the answer in unsigned 16-bit hexadecimal format.
<br>
a.  
&nbsp; 0xE76D (unsigned 16-bit)  
+ &nbsp; &nbsp; 0x14 (2's comp 8-bit)  
<br>
<br>	1110 0111 0110 1101 + 0000 0000 0001 0100 = 
<br> 	1110 0111 1000 0001 
<br>	= E781
<br>	No overflow
<br>
b.  
&nbsp; BD72 (unsigned 16-bit)  
- &nbsp; &nbsp; D6 (2's comp 8-bit)  
<br>
<br> 1011 1101 0111 0010 - 0000 0000 1101 0110 = 
<br> 1011 1100 1001 1000
<br> = B C 9 8
<br> No Overflow.
<br>
</ol>
## Part 5: Digital Logic

Objectives:

- Know how to use bit masking to change registers. 
- Demonstrate the ability to design a digital logic circuit.
<br>
<br>

<ol start="8">
<li> (5 pts)  You are given a 16-bit register called R6 that contains a certain value.  You need to ensure that the 0th and the 6th bits are set (in other words, they have a value of one) in order for your program to function correctly, but you don't want to change any other bits in the register.  Describe what operation needs to be performed and the operands involved in order to set these bits.  Pseudocode will suffice.</li>
<br>
<br> R6 = R6 OR 0000 0000 0010 0001
<br> R6 = R6 OR 0x0021
<br>
<br>
<li>(5 pts) You forgot you needed to modify one other 16-bit register called R7 to allow your program to work correctly.  In R7, the 0th, 6th, and 15th bits need to be cleared (set to zero) without affecting the other bits in the register.  Describe what operation needs to be performed and the operands involved in order to clear these bits.  Pseudocode will suffice.</li>
<br>
<br> R7 = R7 AND 1011 1111 1101 1110
<br> R7 = R7 AND 0xBFDE
<br>
<br>
<li>(5pts) Draw a schematic for a circuit that will generate the following equation.  You may use only the following gates: AND, OR, and NOT.  **No simplification is allowed**.

F = X'Y' + YZ' +X'Y'Z</li>
</ol>
<br>
<br>

![SR](SR.PNG)
<br>
<br>
<br>
<br>