;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			MOV		#20, r8				; Store 20 into r8
			CLR		r9					; Clear r9
   			CMP		#0x1234, &0x0216	; Compare 0x1234 against value at 0x0216
   			JEQ		LESSTHAN
   			JL		LESSTHAN  			; If less, jump to LESSTHAN
MORETHAN	ADD		r8, r9				; Add r8 into r9
			DEC		r8					; Decrease r8
			JZ		END					; If its 0, jump to the end
			JMP		MORETHAN			; Repeat loop
LESSTHAN	CMP		#0X1000, &0x0216    ; Compare 0x1000 against value at 0x0216
			JEQ		LESSTHAN
			JL		SMALL				; If less, jump to SMALL
BIG			ADD		#0xEEC0, &0x0216	; Add 0xEEC0 into 0x0212
			MOV.B	R2, &0x0202			; Store carry flag as a byte into 0x0202
			JMP		END					; Go to END
SMALL		MOV		&0x0216, r6			; Move value at address 0x0216 into r6
			AND		#1, r6				; And 0001 and r6 value
			JZ		DIVIDE				; If zero, go to divide, otherwise move to DIVIDE
MULTIPLY	RLA		&0x0216				; Multiply by 2
			JMP		STORE				; Go to end
DIVIDE		RRA		&0x0216				; Divide by 2
STORE		MOV 	&0x0216, &0x0212	; Stores value of 0x0216 into 0x0212
END			JMP		END					; CPU trap
;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
