;-------------------------------------------------------------------------------
; C2C Zach Martinez
; ECE 382 - Capt Warner
; Lab 1
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory

Message		.byte		0xef,0xc3,0xc2,0xcb,0xde,0xcd,0xd8,0xd9,0xc0,0xcd,0xd8,0xc5,0xc3,0xc2,0xdf,0x8d,0x8c,0x8c,0xf5,0xc3,0xd9,0x8c,0xc8,0xc9,0xcf,0xde,0xd5,0xdc,0xd8,0xc9,0xc8,0x8c,0xd8,0xc4,0xc9,0x8c,0xe9,0xef,0xe9,0x9f,0x94,0x9e,0x8c,0xc4,0xc5,0xc8,0xc8,0xc9,0xc2,0x8c,0xc1,0xc9,0xdf,0xdf,0xcd,0xcb,0xc9,0x8c,0xcd,0xc2,0xc8,0x8c,0xcd,0xcf,0xc4,0xc5,0xc9,0xda,0xc9,0xc8,0x8c,0xde,0xc9,0xdd,0xd9,0xc5,0xde,0xc9,0xc8,0x8c,0xca,0xd9,0xc2,0xcf,0xd8,0xc5,0xc3,0xc2,0xcd,0xc0,0xc5,0xd8,0xd5,0x8f
EndMessage
Key			.byte		0xac
EndKey
Store		.word		0x0200

            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------

            ;
            ; load registers with necessary info for decryptMessage here
            ;

			mov		#Message, R12
			mov		#Key, R13
			mov		Store, R14

            call    #decryptMessage

CPUtrap:    jmp     CPUtrap

;-------------------------------------------------------------------------------
                                            ; Subroutines
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
; Subroutine Name: decryptMessage
; Author:
; Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;            the address of the encrypted message, address of the key, and address
;            of the decrypted message (pass-by-reference).  Accepts the length of
;            the message by value.  Uses the decryptCharacter subroutine to decrypt
;            each byte of the message.  Stores the results to the decrypted message
;            location.
; Inputs:Address of the message, Address of the key, Address to store the result
; Outputs:Decrypted message
; Registers destroyed:R12,R13,R14
;-------------------------------------------------------------------------------

decryptMessage:

			;while Message != EndMessage
			;	decryptCharacter(*Message, *Key)
			;	Key++
			;	Message++
			;	if Key = endKey
			;		Key = StartKey   ; startkey = location in rom, key = R14

			pop 	R10
Decrypt		cmp.b		#EndMessage, R12
			jeq		ExitFcn
			push.b	0(R12)
			push.b	0(R13)
			call	#decryptCharacter

			inc		R12
			inc		R13
			cmp.b	EndKey, 0(R13)
			jne		Continue
 			mov		#Key, R13
Continue	jmp		Decrypt

ExitFcn		push	R10
			ret


;-------------------------------------------------------------------------------
; Subroutine Name: decryptCharacter
; Author:
; Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;            decrypted byte in the same register the encrypted byte was passed in.
;            Expects both the encrypted data and key to be passed by value.
; Inputs: Value of the Key, Value of the Message, Result storage location
; Outputs:The result of the decryption
; Registers destroyed:R9
;-------------------------------------------------------------------------------

decryptCharacter:
			pop		R9		; return address
			pop.b 	R7		; Key
			pop.b	R8		; Message

			xor		R7, R8

			mov.b	R8, 0(R14)
			inc		R14
			push	R9
            ret


;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
