# Lab 2 - Subroutines - "Cryptography"
#

## By C2C Zach Martinez
 
### Objectives
The goal of this lab is to practice assembly programming skills by using subroutines to execute a decryption program.

### Preliminary design

To begin, I created psuedocode that outlined the basic structure of how the program should functions. 


	main
		decryptMessage
		CPUTrap

	decryptByte(encryptedByte,keyByte)
		Get(encyptedByte)
		Get(keyByte)
		unencryptedByte = Xor(encryptedByte, keyByte)
		Return unencryptedByte

	decryptMessage(lengthOfKey)
		byteExtensiton = BitShiftLeft('0', lengthOfKey)
		*keyByte = *Rom.keyByte
		For(int x = 0; x<message.length;x++)
			*encryptedByte = *Rom.Message(x)
		 	*encryptedByte= XOR(byteExtension, encryptedByte)
			Ram.Unencrypted(x) = decryptByte(encryptedByte, keyByte)

for the XOR and the BitShiftLeft functions, I will use assembly commands as opposed to creating their own functions manually. 

each of the variables listed in the psuedocode will be assigned to registers as listed below

##### Required Registers
	
	R4 = encryptedByte - hold value of encrypted Byte
	R5 = keybyte - hold value of the cipher key
	R6 = unencryptedByte - hold value of the unencrypted Byte
	R7 = lengthOfKey - hold value of the length of the key if given
	R8 = byteExtension - hold value of the encrypted byte after being extended
	R9 = *keyByte - pointer to keyByte in ROM
	R10 = *encryptedByte - pointer to encryptedByte in ROM

##### Design Limitations
This current build does not account for a lack of a key nor does have implementation for an unknown length of characters.

### Testing Methodology
To test the code I will have a string that I will convert into ASCII characters beforehand. Then, I will run the string through my program to see if the output is the same as what I expected. Next, I will run the string using a simple key and see if the output changes as expected.

### Design Changes
In the final version of my program, I changed the register values to fit in with the standard practices for functions. I also accounted for the fact that the return address is pushed to the stack by popping those values into registers.

	R7 = Key value
	R8 = Message Value
	R9 = Addresss to return to decrypt message fcn
	R10 = Address to return to the main
	R12 = Address to the messsage
	R13 = Address to the key
	R14 = Address to store the result

	

### Documentation
None