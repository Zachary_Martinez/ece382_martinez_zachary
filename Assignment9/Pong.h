/*--------------------------------------------------------------------
Name: C2C Zach Martinez
Date: 5 Sep 2017
Course: ECE 382
File: pong.h
Event: Assignment 7 - Pong

Purp: Implements a subset of the pong game

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef _PONG_H
#define _PONG_H

#include <stdint.h>


#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT 320

typedef struct {
	int16_t x;
	int16_t y;
} vector2d_t;

typedef struct {
    vector2d_t position;
    vector2d_t velocity;
    unsigned char radius;
} ball_t;

ball_t createBall(int16_t xPos, int16_t yPos, int16_t xVel, int16_t yVel, int8_t radius);

void bounceBall(ball_t *ballToMove, ball_t *paddleToUse);

#endif
