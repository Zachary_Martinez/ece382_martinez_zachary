/*--------------------------------------------------------------------
Name: C2C Zach Martinez
Date: 13 Oct 2017
Course: ECE 382
File: main.c
Event:Lab 4 - Pong

Purp: Implements pong and etch a sketch. no bonus completed

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include <stdbool.h>
#include "color.h"
#include "Pong.h"

extern void initMSP();
extern void initLCD();
extern void clearScreen();
extern void Delay40ms();
extern void Delay160ms();
extern void drawBox(int16_t xPos, int16_t yPos, int16_t color);
extern void eraseBox(int16_t xPos, int16_t yPos);
extern void drawPaddle(int16_t xPos, int16_t yPos, int16_t color);

void checkCollision(ball_t * myBall, ball_t * myPaddle);
void moveBox(int16_t *xPos, int16_t *row, int16_t LR);
void movePaddle(int16_t *xPos, int16_t *yPos);

// Button press (active low) defines
#define		UP_BUTTON		!(P2IN & BIT2)
#define		DOWN_BUTTON		!(P2IN & BIT1)
#define		LEFT_BUTTON		!(P2IN & BIT0)
#define		RIGHT_BUTTON	!(P2IN & BIT3)
#define		SWITCH_BUTTON	!(P1IN & BIT3)
#define		STEP_SIZE		10

char interruptFlag = 0;

void main() {
	// === Initialize system ================================================
	IFG1 = 0; /* clear interrupt flag1 */
	WDTCTL = WDTPW+WDTHOLD; /* stop WD */


	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	TA0CCR0 = 0x8000;							// create a 16ms roll-over period
	TA0CTL &= ~TAIFG;							// clear flag before enabling interrupts = good practice
	TA0CTL = ID_3 | TASSEL_2 | MC_1 | TAIE;		// Use 1:8 prescalar off MCLK and enable interrupts
	_enable_interrupt();

	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();

	// Create variables to use
	volatile int16_t eraseOrDraw = 1;
	volatile int16_t leftRight = 0;
	static const int16_t fgColor = COLOR_16_GOLD;
	ball_t myBall = createBall(100, 200, 5, 3, 5);
	ball_t myPaddle = createBall(0, 100, 0, 0, 5);

	// This while loop runs the Pong game and exits on S1 actuation
	clearScreen();
	drawPaddle(myPaddle.position.x, myPaddle.position.y, fgColor);
	while(!SWITCH_BUTTON){
		if(interruptFlag){
		if(UP_BUTTON){
			eraseBox(myPaddle.position.x, myPaddle.position.y+40);
			moveBox(&myPaddle.position.x, &myPaddle.position.y, 0);
			drawPaddle(myPaddle.position.x, myPaddle.position.y, fgColor);
		}
		else if(DOWN_BUTTON){
			eraseBox(myPaddle.position.x, myPaddle.position.y);
			moveBox(&myPaddle.position.x, &myPaddle.position.y, 0);
			drawPaddle(myPaddle.position.x, myPaddle.position.y, fgColor);
		}
		if(myBall.velocity.x != 0){
			eraseBox(myBall.position.x, myBall.position.y);
			bounceBall(&myBall, &myPaddle);
		}
		drawBox(myBall.position.x, myBall.position.y, fgColor);
		interruptFlag=0;
		}
	}

	// This while loop runs the Etch-a-Sketch functionality
	clearScreen();
	while(1) {
		if(interruptFlag){
			if(SWITCH_BUTTON){
				eraseOrDraw = eraseOrDraw * -1;
			}
			if(eraseOrDraw == 1)
				eraseBox(myBall.position.x, myBall.position.y);

			moveBox(&myBall.position.x, &myBall.position.y, 1);
			drawBox(myBall.position.x, myBall.position.y, fgColor);
			interruptFlag=0;
		}
	}
}

/*-------------------------------------------------------------------------------
	Name: moveBox
	Inputs: *x = x coordinate, *y = y coordinate,
				leftRight = enables left/right functions
	Outputs: none
	Purpose: takes two coordinates and adds the step size to them based on the
				button that is pressed.
-------------------------------------------------------------------------------*/
void moveBox(int16_t *x, int16_t *y, int16_t leftRight){
	if (UP_BUTTON){
		if(*y - STEP_SIZE <= 5)
			*y = 1;
		else
			*y = *y - STEP_SIZE;
	}
	if (DOWN_BUTTON){
		if(leftRight == 1){
			if(*y + STEP_SIZE >= (SCREEN_HEIGHT-10))
				*y = (SCREEN_HEIGHT-10);
			else
				*y = *y + STEP_SIZE;
		}
		if(leftRight == 0){
			if(*y + STEP_SIZE >= (SCREEN_HEIGHT-50))
				*y = (SCREEN_HEIGHT-50);
			else
				*y = *y + STEP_SIZE;
		}
	}
	if(leftRight == 1){
		if (LEFT_BUTTON){
			if(*x - STEP_SIZE <= 5)
				*x = 1;
			else
				*x = *x - STEP_SIZE;
		}
		if (RIGHT_BUTTON){
			if(*x + STEP_SIZE >= (SCREEN_WIDTH-10))
				*x = (SCREEN_WIDTH-10);
			else
				*x = *x + STEP_SIZE;
		}
	}
}

/*-------------------------------------------------------------------------------
	Name: createBall
	Inputs: xPos = x coordinate, yPos = y coordinate, xVel = x step size,
				yVel = y step size, radius = size of the ball
	Outputs: a ball struct that contains all the input variables.
	Purpose: takes all of the inputs and stores them within a single struct.
-------------------------------------------------------------------------------*/
ball_t createBall(int16_t xPos, int16_t yPos, int16_t xVel, int16_t yVel, int8_t radius){
	ball_t myBall;
	myBall.position.x = xPos;
	myBall.position.y = yPos;
	myBall.velocity.x = xVel;
	myBall.velocity.y = yVel;
	myBall.radius = radius;
	return myBall;
}


/*-------------------------------------------------------------------------------
	Name: checkCollision
	Inputs: myBall = the ball in play, myPaddle = paddle to bounce the ball off
	Outputs: none
	Purpose: checks to see if the ball hits a boundary or the paddle. otherwise,
				it stops moving
-------------------------------------------------------------------------------*/
void checkCollision(ball_t * myBall, ball_t *myPaddle){

	int16_t *xPos = &(*myBall).position.x;
	int16_t *yPos = &(*myBall).position.y;
	int16_t *xVel = &(*myBall).velocity.x;
	int16_t *yVel = &(*myBall).velocity.y;
	int16_t *paddle = &(*myPaddle).position.y;

	if(*xPos + *xVel >= (SCREEN_WIDTH-10))
		*xVel = *xVel * -1;

	else if(*xPos + *xVel <= 0){
		if((*yPos >= *paddle-9) && (*yPos <= (*paddle+49)))
			*xVel = *xVel * -1;
		else{
			*xVel = 0;
			*yVel = 0;
		}
	}

	if(*yPos + *yVel >= (SCREEN_HEIGHT-10))
		*yVel = *yVel * -1;

	else if(*yPos + *yVel <= 0)
		*yVel = *yVel * -1;
}

/*-------------------------------------------------------------------------------
	Name: bounceBall
	Inputs: myBall = the ball in play, myPaddle = paddle to bounce the ball off
	Outputs: none
	Purpose: takes the ball and updates its position while avoid collisions with the wall.
-------------------------------------------------------------------------------*/
void bounceBall(ball_t * myBall, ball_t * myPaddle){
	checkCollision(myBall, myPaddle);
	(*myBall).position.x += (*myBall).velocity.x;
	(*myBall).position.y += (*myBall).velocity.y;
}


#pragma vector = TIMER0_A1_VECTOR				// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	P1OUT ^= BIT6;						// This provides some evidence that we were in the ISR
	TA0CTL &= ~TAIFG;					// See what happens when you do not clear the flag
	interruptFlag=1;
}
