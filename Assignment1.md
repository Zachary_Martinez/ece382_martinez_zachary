# Assignment 1 - Assembly Process, MSP430 Execution, MSP430 Instruction Set

**Name: C2C Zach Martinez**


**Section: M1A**


**Documentation: None**



## Assembly Process

**What does the assembler do?  Be specific.**

	Takes the code that is written by a programer and translates it into object code which is a series of 1s and 0s

**What does the linker do?  Be specific.**

	The linker takes the specified object files that have been created and puts them together to make an executable file.

Consider the following code:
```
mov.w   #0xdfec, &0x0200        ; stores the value 0xdfec at memory location 0x0200
```

**What is the location of each byte of the stored word, assuming big-endian byte ordering?**

	d @ 0x0200
	f @ 0x0204
	e @ 0x0208
	c @ 0x020c

**What byte ordering scheme does the MSP430 use?  What would be the location of each byte of the stored word?**

	Little Endian
	d @ 0x020c
	f @ 0x0208
	e @ 0x0204
	c @ 0x0200

## MSP430 Execution

**What's the purpose of the program counter?  Be specific.**

	to keep track of which instruction is currently being ran, and when incremented, accesses the next instruction available

**Assume `pc` currently holds `0xc000`.  The current instruction is 4 bytes long.  What is the value of `pc` the instant after this step?  Don't over-think this.**

	0xd000

**What happens if you attempt to access a memory address that isn't implemented in your chip?  Talk about reads, writes, and execution.**

	if you try to read, you will get a random value  from the specified location that has no meaning

	if you try to write, you will overwrite an address location and possibly corrupt your program by writing over some command or other memory location

	if you try to execute, you will most likely not have a proper command to execute, or execute an unintended command.	

## MSP430 Instruction Set

**MSP430 Instruction Set.**

Consider the following code:
```
mov.w   #0xbeef, r8
swpb    r8
and.w   #0xff, r8
mov.w   r8, &0x0200
inv.w   r8
dec.w   r8
```

**Show the contents of `r8` after the execution of each instruction.**

	mov.w r8 = beef
	swpb  r8 = efbe
	and.w r8 = efbe
	mov.w r8 = efbe
	inv.w r8 = 1041
	dec.w r8 = 1040