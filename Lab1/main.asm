;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
ADD_OP:     .equ	0x11
SUB_OP:		.equ	0x22
MUL_OP:		.equ	0x33
CLR_OP:		.equ	0x44
END_OP:		.equ	0x55
			.byte	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22
			.byte	0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33
			.byte	0x00, 0x44, 0x33, 0x33, 0x08, 0x55
RESULT		.word	0x0200
OPERAT		.word	0xc000




            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			mov RESULT, r7
			mov OPERAT, r5
			mov OPERAT, r6
			inc r6
			mov.b 0(r5), r8

Operation	cmp.b #ADD_OP, 0(r6)
			jz Adder

			cmp.b #SUB_OP, 0(r6)
			jz Subtractor

			cmp.b #MUL_OP, 0(r6)
			jz Multiplier

			cmp.b #CLR_OP, 0(r6)
			jz Clearer

			cmp.b #END_OP, 0(r6)
			jz Ender
;---Add---

Adder		inc r6
			mov.b 0(r6), r9
			add.b r9, r8
			jc	TooHigh
			jmp ContAdd

TooHigh		mov #0xFF, r8
ContAdd		mov r6, r5
			inc r6
			jmp	Return

;---Sub---

Subtractor	inc r6
			mov.b 0(r6), r9
			sub.b r9, r8
			jn	TooLow
			jmp ContSub

TooLow		clr r8
ContSub		mov r6, r5
			inc r6
			jmp	Return

;---Mul---

Multiplier	inc r6

			mov.b r8, r10
			mov.b 0(r6), r11
			mov.b #0x0, r12

MulLoop		cmp #0x00, r10
			jeq EndLoop
			cmp #0x00, r11
			jeq EndLoop

			mov r10, r13
			and #0x0001, r13
			tst r13
			jz MulStep
			add r11, r12

MulStep		rra r10
			rla r11
			jmp MulLoop

EndLoop		mov r12, r8
			cmp #0xFF, r8
			jge	TooBig
			cmp #0x01, r8
			jl	TooSmall
			jmp ContMul

TooBig		mov #0xFF, r8
			jmp ContMul
TooSmall	clr r8
ContMul		mov r6, r5
			inc r6
			jmp Return	; //TODO: add functionality

;---Clr---
Clearer		inc r6
			clr.b r8
			clr.b r9
			mov r6, r5
			mov.b 0(r5), r8
			inc r6

			mov.b r9, 0(r7)
			inc r7

			jmp	Operation

;---Rtn---
Return		mov.b r8, 0(r7)
			inc r7
			jmp Operation

;---End---
Ender		jmp Ender

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
