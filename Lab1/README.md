# Lab 1 - Assembly Language - "A Simple Calculator"
#

## By C2C Zach Martinez
 
### Objectives
The goal of this lab is to practice assembly programming skills by using the instruction set, addressing modes, conditional jumps, status register flags, assembler directives, and the assembly process in order to create a simple calculator
### Preliminary design

To begin, I created a flow chart to outline how i will organize the program.

![flowchart](FlowChart.PNG)
##### Design Limitations
This current build does not have the ability to calculate numbers larger than 255. only positive signed 2 Byte values are usable. If an operation is input that is not known, the program will move into the addition operation by default.

this program is able to use all numbers between 0x00 and 0xFF as operands for all operations. any results that are outside this range will be capped at 0x00 and 0xFF.



##### Required Registers
	
	R5 = Location of first operand
	R6 = Location of second operand / operation
	R7 = Location for the result to be stored
	R8 = Value of the first operand
	R9 = Value of the second operand
	R10 = value to be multiplied
	R11 = Value to multiply by
	R12 = Result of the multiplication

### Testing Methodology


To test the program, i will use the provided test cases to make sure that the output of the program shows what is expected when the provided inputs are used.

#####Test Cases
###### Required Functionality

	Input:	0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 
			0x22, 0x22, 0x22, 0x11, 0xCC, 0x55
	
	Result: 0x22, 0x33, 0x00, 0x00, 0xCC

###### B Functionality

	Input:	0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 
			0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 
			0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55

	Result: 0x22, 0x33, 0x44, 0xFF, 0x00, 0x00, 
			0x00, 0x02

###### A Functionality

	Input:	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 
			0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 
			0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 
			0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 
			0x08, 0x55

	Result: 0x44, 0x11, 0x88, 0x00, 0x00, 0x00, 
			0xff, 0x00, 0xff, 0x00, 0x00, 0xff




### Design

when writing the code for this calculator, I was able to keep the code modular in the same fashion as the flowchart.

In the ROM, the operations, the instructions and the address locations for the operations and results are initialized into variables. this provides some abstraction that allowed me to make the code more readable. 


![Lab1Vars](Lab1Vars.PNG)

Variables for the operand locations are then initialized, the operation is determined, the selected operation is evaluated, the result is stored, and the program repeats until the end process is used.


### Debugging

When writing this code, I came across many syntax errors. 1st I had many instances where my use of indexed addressing mode was incorrect. I first used symbolic addressing mode for my destination operands. after referring to the data-sheet, I relearned that symbolic addressing mode cannot be used for destinations. I then used indexed addressing mode. I wrote it as (0)R5 in many places when the correct format is 0(R5). Finally, I had an issue of not using the jump commands correctly by not having a return command for some of them. this resulted in me jumping to a location, and then running into other commands afterwards that I did not intend to run.

### Results
The final version of this program is fully able to 
do add, subtract, multiply, clear, and end operations. It also prevents values outside 0x00 and 0xFF from being stored.
all the test cases work as described and the program does not have

![Lab1Result](Lab1Result.PNG)

### Observations

Editing the program while the debugger is running does not change the program in all cases, and a fresh build is recommended after each change is made. 

### Documentation
Referred to online sources in order to figure out the algorithm for the multiplication using bit-wise operations.