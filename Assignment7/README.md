# Assignment 7 - Pong

## Pong in C

Write a C program that implements a subset of the functionality of the video "pong" game.

- Define the height/width of the screen as constants.
- Create a structure that contains the ball's parameters (x/y position, x/y velocity, radius).  Note: you may want to use more than one structure (i.e. nested structures) to do this.
- Make a function that creates a ball based on parameters passed into it.
- Make another function that updates the position of the ball (input is a ball structure, output is the updated ball structure).
    - This function must handle the "bouncing" of the ball when it hits the edges of the screen.
    - When it hits an edge, you flip the sign on the x or y velocity to make the ball move a different direction.
    - This function should call four other "collision detection" functions; one for each of the screen edges.
    - The "collision detection" functions return a `char` (1 for true, 0 for false - `#define` these values) to indicate whether or not there is a collision.
    - You may consolidate collision detection to one function if you choose.
- You must create three separate files: header, implementation, and your `main.c`.

## Turn-In Requirements (Bitbucket)

- Source code files (`main.c`, header, and implementation).
- Simulator screenshots (show pictures before and after collisions for all four walls)

![Start](Start.PNG)
![FlipTop](FlipTop.PNG)
![FlipRight](FlipRight.PNG)
![FlipBot](FlipBot.PNG)
![FlipLeft](FlipLeft.PNG)

- Explain what you are showing the reader in the screenshots.
- Answers to the following questions in readme.md file:
    - How did you verify your code functions correctly?
	    - I checked the variables table and observed the value of the balls position after each  iteration "moveBall" function is called. before the ball reached either 0 or the value of the screenwidth/height in the x or y direction respectively, the velocity flipped, making the ball move back from where it came.
    - How could you make the "collision detection" helper functions only visible to your implementation file (i.e. your `main.c` could not call those functions directly)?
	    - I did not initialize the functions the header file, meaning only the implementation file could access the function.