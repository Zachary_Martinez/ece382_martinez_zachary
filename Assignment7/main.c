/*--------------------------------------------------------------------
Name: C2C Zach Martinez
Date: 5 Sep 2017
Course: ECE 382
File: main.c
Event: Assignment 7 - Pong

Purp: Implements a subset of the pong game

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h> 
#include "Pong.h"



int main(void) {
    WDTCTL = WDTPW | WDTHOLD;
	volatile int xPos = 220;
	volatile int yPos = 300;
	volatile int xVel = 7;
	volatile int yVel = 15;
	volatile unsigned char radius = 5;
	ball_t myBall = createBall(xPos, yPos, xVel, yVel, radius);
    while(1){
    	moveBall(&myBall);
    }
}

ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius){
	ball_t myBall;
	myBall.position.x = xPos;
	myBall.position.y = yPos;
	myBall.velocity.x = xVel;
	myBall.velocity.y = yVel;
	myBall.radius = radius;
	return myBall;
}

ball_t * checkCollision(ball_t * myBall){
	if((*myBall).position.x + (*myBall).velocity.x > SCREEN_WIDTH){
		(*myBall).velocity.x = (*myBall).velocity.x * -1;
	}
	else if((*myBall).position.x + (*myBall).velocity.x < 0){
		(*myBall).velocity.x = (*myBall).velocity.x * -1;
	}

	if((*myBall).position.y + (*myBall).velocity.y > SCREEN_HEIGHT){
		(*myBall).velocity.y = (*myBall).velocity.y * -1;
	}
	else if((*myBall).position.y + (*myBall).velocity.y < 0){
		(*myBall).velocity.y = (*myBall).velocity.y * -1;
	}
	return myBall;
}

ball_t * moveBall(ball_t * myBall){
	checkCollision(myBall);
	(*myBall).position.x += (*myBall).velocity.x;
	(*myBall).position.y += (*myBall).velocity.y;

	return myBall;
}



