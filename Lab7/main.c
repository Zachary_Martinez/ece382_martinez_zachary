#include <msp430.h>

#define DELAY_CYCLES 500000
#define		HIGH_2_LOW		P2IES |= BIT0
#define		LOW_2_HIGH		P2IES &= ~BIT0
int	left = 1;
int right = 0;
int timer = 0;
int counter = 0;
int stop = 0;
int wallFront=0;
int wallLeft=0;
int wallRight=0;

int	pin;
int	pulseDuration;
int Wall =0;
void goRobot(int turnLeft, int turnRight, int turnSpeed, int turnTime);
void initMSP();
void moveServo(int direction);
void checkWalls();
void checkDistance();

void main(void){
        initMSP();
        _enable_interrupt();
        while (1)
        {
        	wallFront =0;
        	wallLeft =0;
        	wallRight =0;
        	checkDistance();
        	moveServo(1);
    	    __delay_cycles(DELAY_CYCLES);
    	    if (stop == 0){
    	    	goRobot(0,0,300,10);	//Forward
    	    }
    	    if (stop == 1){
            	moveServo(0);
        	    __delay_cycles(DELAY_CYCLES);
            	moveServo(2);
        	    __delay_cycles(DELAY_CYCLES);
            	if(wallRight ==1){
                	goRobot(1,0,400,10);	//90left
            	}
            	if(wallLeft == 1){
                	goRobot(0,1,300,10);	//90right
            	}
    	    }
        }
        	//goRobot(0,0,300,10);	//back
        	//goRobot(0,1,300,10);	//90right
        	//goRobot(1,0,300,10);	//90left
        	//goRobot(0,1,350,10);	//45right
        	//goRobot(1,0,350,10);	//45left

}

void initMSP(){
	WDTCTL = WDTPW|WDTHOLD;  		// stop the watchdog timer

    P1DIR |= 0xFF;                	// Turn all port 1 pins on for output

    TA1CTL |= TASSEL_2|MC_1|ID_0; // configure Timer 1 (SMCLK, UP mode)

    // PWM setup
    TA1CCR0 = 1000;               // set signal period to 1000 clock cycles (~1 millisecond)
    TA1CCTL0 |= CCIE;             // enable CC interrupts
    TA1CCR1 = 300;                // set duty cycle to 300/1000 (25%)
    TA1CCTL1 |= OUTMOD_7|CCIE;    // set TACCTL1 to Reset / Set mode//enable CC interrupts

    //clear capture compare interrupt flags
    TA1CCTL0 &= ~CCIFG;
    TA1CCTL1 &= ~CCIFG;

    TA0CTL |= TASSEL_2|MC_1|ID_0; // configure Timer 0 (SMCLK, UP mode)

    // PWM setup
    TA0CCR0 = 20000;               // set signal period to 20000 clock cycles (~20 millisecond)
    TA0CCTL0 |= CCIE;             // enable CC interrupts
    TA0CCR1 = 600;                // set duty cycle to 600/20000 (3%)
    TA0CCTL1 |= OUTMOD_7|CCIE;    // set TACCTL1 to Reset / Set mode//enable CC interrupts

    //clear capture compare interrupt flags
    TA0CCTL0 &= ~CCIFG;
    TA0CCTL1 &= ~CCIFG;

    P2IFG &= ~BIT0;						// Clear any interrupt flag on P2.3
    P2IE  |= BIT0;						// Enable P2.3 interrupt
   	HIGH_2_LOW;
}

void goRobot(int turnLeft, int turnRight, int turnSpeed, int turnTime){
	timer = 0xFFFF;
    counter = turnTime;

    left = turnLeft;
    right = turnRight;

    TA1CTL &= ~MC_1;
    TA1CCR1 = turnSpeed;
    TA1CTL |= MC_1;

	while (counter>=0){
     	counter--;
       	timer = 0XFFFF;
       	while(timer>=0)
      		timer--;
    }
}

void moveServo(int direction){
	if(direction ==0){
		TA0CCR1 = 600;
	}
	if(direction ==1){
		TA0CCR1 = 1600;
	}
	if(direction ==2){
		TA0CCR1 = 2600;
	}

	checkWalls();
	//TA0CCR1 += 1000;
	//if(TA0CCR1>2600){
	//	TA0CCR1 = 600;
	//}

}
void checkWalls(){
	if(Wall ==0){
		P1OUT &= ~BIT0;
		P1OUT &= ~BIT6;
		stop = 0;
	}
	else if(TA0CCR1 ==2600){
		P1OUT |= BIT0;
		P1OUT &= ~BIT6;
		wallLeft =1;
		stop = 0;
	}
	else if(TA0CCR1 == 600){
		P1OUT &= ~BIT0;
		P1OUT |= BIT6;
		wallRight=1;
		stop = 0;
	}
	else{
		P1OUT |= BIT0;
		P1OUT |= BIT6;
		wallFront = 1;
		stop = 1;
	}

}

void checkDistance(){
	P1OUT|= BIT4;
	P1OUT|= BIT4;
	P1OUT|= BIT4;
    P1OUT&= ~BIT4;
}

#pragma vector = TIMER1_A0_VECTOR            // TA1CCR0 CCIFG vector
__interrupt void TA1captureCompare0_ISR (void) {
	if(left == 1){
		P1OUT |= BIT2;                        //Turn on LED
		P1OUT &= ~BIT1;                        //Turn on LED
	}
	if(left == 0){
		P1OUT &= ~BIT2;                        //Turn on LED
		P1OUT |= BIT1;                        //Turn on LED
	}
	if(right == 0){
		P1OUT |= BIT5;                        //Turn on LED
		P1OUT &= ~BIT7;                        //Turn on LED
	}
	if(right == 1){
		P1OUT &= ~BIT5;                        //Turn on LED
		P1OUT |= BIT7;                        //Turn on LED

	}

	if (stop ==	1){
		P1OUT &= ~BIT2;                        //Turn on LED
		P1OUT &= ~BIT1;                        //Turn on LED
		P1OUT &= ~BIT5;                        //Turn on LED
		P1OUT &= ~BIT7;                        //Turn on LED
	}

    TA1CCTL1 &= ~CCIFG;                  //clear capture compare interrupt flag
}

#pragma vector = TIMER1_A1_VECTOR            // TA1 CCR2 and CCR1 CCIFG, TAIFG vector
__interrupt void TA1captureCompare1_ISR (void) {
	if(left == 1){
		P1OUT &= ~BIT2;                        //Turn on LED
		P1OUT |= BIT1;                        //Turn on LED
	}
	if(left == 0){
		P1OUT |= BIT2;                        //Turn on LED
		P1OUT &= ~BIT1;                        //Turn on LED
	}
	if(right == 0){
		P1OUT &= ~BIT5;                        //Turn on LED
		P1OUT |= BIT7;                        //Turn on LED
	}
	if(right == 1){
		P1OUT |= BIT5;                        //Turn on LED
		P1OUT &= ~BIT7;                        //Turn on LED
	}

	if (stop == 1){
		P1OUT &= ~BIT2;                        //Turn on LED
		P1OUT &= ~BIT1;                        //Turn on LED
		P1OUT &= ~BIT5;                        //Turn on LED
		P1OUT &= ~BIT7;                        //Turn on LED
	}

    TA1CCTL1 &= ~CCIFG;                   //clear capture compare interrupt flag
}

#pragma vector = TIMER0_A0_VECTOR            // TA1CCR0 CCIFG vector
__interrupt void TA0captureCompare0_ISR (void) {
	P1OUT|= BIT3;
    TA0CCTL1 &= ~CCIFG;                  //clear capture compare interrupt flag
}

#pragma vector = TIMER0_A1_VECTOR            // TA1 CCR2 and CCR1 CCIFG, TAIFG vector
__interrupt void TA0captureCompare1_ISR (void) {

	P1OUT &= ~BIT3;                        //Turn on LED
    TA0CCTL1 &= ~CCIFG;                   //clear capture compare interrupt flag
}

#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {
	if (P2IN & BIT0)
		pin=1;
	else
		pin=0;

	switch (pin) {
		case 0:
			pulseDuration = TA0R;
			LOW_2_HIGH;
			break;
		case 1:
			if(pulseDuration>=2500){
				Wall = 0;
			}
			if(pulseDuration<2500){
				Wall = 1;
			}
			TA0R = 0x0000;
			HIGH_2_LOW;
			checkWalls();

	} // end switch
	P2IFG &= ~BIT0;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

